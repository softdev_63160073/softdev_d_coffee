/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.dcoffee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Acer
 */
public class UpdateDatabase {
    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:d_coffee.db";
        //Connection database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        
        String sql ="UPDATE category SET cat_name=? WHERE cat_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,"Candy");
            stmt.setInt(2, 3);
            
            int  status = stmt.executeUpdate();
           /* ResultSet key = stmt.getGeneratedKeys();
            key.next();
            System.out.println(""+key.getInt(1)); */
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        //Close database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
