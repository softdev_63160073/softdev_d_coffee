/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.dcoffee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class InsertDatabase {
    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:d_coffee.db";
        //Connection database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        
        String sql ="INSERT INTO category(cat_id, cat_name) VALUES (?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,3);
            stmt.setString(2, "candy");
            int  status = stmt.executeUpdate();
           /* ResultSet key = stmt.getGeneratedKeys();
            key.next();
            System.out.println(""+key.getInt(1)); */
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        //Close database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
