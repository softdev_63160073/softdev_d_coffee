/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.dcoffee;

import com.thanadon.dcoffee.model.User;
import com.thanadon.dcoffee.service.UserService;

/**
 *
 * @author Acer
 */
public class TestUserService {

    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("thanadon", "123123");
        if (user != null) {
            System.out.println("Welcome user " + user.getName());
        } else {
            System.out.println("Error");
        }
    }
}
