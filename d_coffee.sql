--
-- File generated with SQLiteStudio v3.2.1 on อา. ก.ย. 18 15:52:08 2022
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
DROP TABLE IF EXISTS category;

CREATE TABLE category (
    cat_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    cat_name TEXT (50) NOT NULL
);

INSERT INTO category (
                         cat_id,
                         cat_name
                     )
                     VALUES (
                         1,
                         'Coffee'
                     );

INSERT INTO category (
                         cat_id,
                         cat_name
                     )
                     VALUES (
                         2,
                         'Dessert'
                     );

INSERT INTO category (
                         cat_id,
                         cat_name
                     )
                     VALUES (
                         3,
                         'Candy'
                     );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE product (
    prod_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    prod_name        TEXT (50) UNIQUE,
    prod_price       DOUBLE    NOT NULL,
    prod_size        TEXT (5)  DEFAULT SML
                               NOT NULL,
    prod_sweet_level TEXT (5)  DEFAULT (123) 
                               NOT NULL,
    prod_type        TEXT (5)  DEFAULT HCF
                               NOT NULL,
    cat_id           INTEGER   REFERENCES category (cat_id) ON DELETE RESTRICT
                                                            ON UPDATE RESTRICT
);

INSERT INTO product (
                        prod_id,
                        prod_name,
                        prod_price,
                        prod_size,
                        prod_sweet_level,
                        prod_type,
                        cat_id
                    )
                    VALUES (
                        1,
                        'Mocca',
                        30.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        prod_id,
                        prod_name,
                        prod_price,
                        prod_size,
                        prod_sweet_level,
                        prod_type,
                        cat_id
                    )
                    VALUES (
                        2,
                        'Espresso',
                        30.0,
                        'SML',
                        '012',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        prod_id,
                        prod_name,
                        prod_price,
                        prod_size,
                        prod_sweet_level,
                        prod_type,
                        cat_id
                    )
                    VALUES (
                        3,
                        'Cookie',
                        40.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        prod_id,
                        prod_name,
                        prod_price,
                        prod_size,
                        prod_sweet_level,
                        prod_type,
                        cat_id
                    )
                    VALUES (
                        4,
                        'Butter Cake',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE user (
    user_id       INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    user_name     TEXT (50) UNIQUE,
    user_gender   TEXT (3)  NOT NULL,
    user_password TEXT (50) NOT NULL,
    user_role     INTEGER   NOT NULL
);

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     1,
                     'thanadon',
                     'M',
                     '123123',
                     1
                 );

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     3,
                     'user2',
                     'F',
                     'password',
                     2
                 );

INSERT INTO user (
                     user_id,
                     user_name,
                     user_gender,
                     user_password,
                     user_role
                 )
                 VALUES (
                     4,
                     'user3',
                     'F',
                     'passwrod',
                     2
                 );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
